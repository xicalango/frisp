
use std::io::stdin;
use std::path::PathBuf;
use libfrisp::env::{Env, Environment};
use libfrisp::value::{Value, ConstVal};

#[derive(Debug, Default)]
enum Mode {
    #[default]
    Interactive,
    RunScript(PathBuf),
}

#[derive(Debug, Default)]
struct Opts {
    includes: Vec<PathBuf>,
    mode: Mode,
    args: Vec<String>,
}

impl Opts {

    pub fn from_env() -> Opts {

        let mut includes = Vec::new();
        let mut mode = Mode::default();
        let mut args = Vec::new();

        let mut opts = std::env::args();

        let mut arguments_done = false;

        while let Some(arg) = opts.next() {

            match (arguments_done, arg.as_str()) {
                (false, "-i") => {
                    let include_file = opts.next().unwrap();
                    includes.push(include_file.into());
                },
                (false, "-m") => {
                    let module_file = opts.next().unwrap();
                    mode = Mode::RunScript(module_file.into());
                },
                (false, "--") => {
                    arguments_done = true;
                }
                (_, _) => {
                    args.push(arg);
                }
            }

        }

        Opts {
            includes,
            mode,
            args,
        }
    }

}

fn run_repl(env: &mut Environment) {
    loop {
        let mut input = String::new();
        let count = stdin().read_line(&mut input).unwrap();

        if count == 0 {
            break;
        }

        match libfrisp::run_with_env(&input, env) {
            Ok(Value::Unit) => {},
            Ok(v) => println!("{v}"),
            Err(e) => println!("{e}"),
        }
    }
}

fn main() {
    let opts = Opts::from_env();

    let mut env = Environment::with_default_content();

    for include in &opts.includes {
        libfrisp::eval_file_with_env(include, &mut env).unwrap();
    }

    env.insert_var("cli-args", ConstVal::from(opts.args.iter().map(Value::string).collect::<Value>()));
    env.insert_var("sys-env", ConstVal::from(std::env::vars().map(|(a, b)| Value::List(vec![Value::string(a),
Value::string(b)])).collect::<Value>()));

    match &opts.mode {
        Mode::Interactive => { run_repl(&mut env); }
        Mode::RunScript(script) => { libfrisp::eval_file_with_env(script, &mut env).unwrap(); }
    }


}





